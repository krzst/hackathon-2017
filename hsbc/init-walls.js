AFRAME.registerComponent('automove-controls', {
    init: function () {
        this.speed = 0.2;
        this.isMoving = true;
        this.velocityDelta = new THREE.Vector3();
    },
    isVelocityActive: function () {
        return this.isMoving;
    },
    getVelocityDelta: function () {
        this.velocityDelta.z = this.isMoving ? -this.speed : 0;
        return this.velocityDelta.clone();
    }
});


document.querySelector('a-scene').addEventListener('render-target-loaded', function () {
    var WALL_SIZE = 5,
        WALL_HEIGHT = 3;
    var el = document.querySelector('#walls');
    var wall;
    var wallTxt;

    for (var x = 0; x < map.height; x++) {
        for (var y = 0; y < map.width; y++) {

            var i = y * map.width + x;
            var position = (x - map.width / 2) * WALL_SIZE + ' ' + 1.5 + ' ' + (y - map.height / 2) * WALL_SIZE;
            if (map.data[i] === 1) {
                // Create wall
                wall = document.createElement('a-box');

                wall.setAttribute('id', 'wall-' + x + '-' + y);
                el.appendChild(wall);
                wall.setAttribute('color', '#fff');
                wall.setAttribute('material', 'src: #texture-wall;');
                wall.setAttribute('width', WALL_SIZE);
                wall.setAttribute('height', WALL_HEIGHT);
                wall.setAttribute('depth', WALL_SIZE);
                wall.setAttribute('position', position);
                wall.setAttribute('static-body', '');
                //wall.setAttribute('class', 'clickable');

                wallTxt = document.createElement('a-entity');
                wallTxt.setAttribute('position', '-2.4 1.3 2.55');
                wallTxt.setAttribute('scale', '0.6 0.6 0.6');
                wallTxt.setAttribute('bmfont-text', 'text: ' + x + '-' + y + ';');

                wall.appendChild(wallTxt);
            }

            if (map.data[i] === 2) {
                // Set player position
                //document.querySelector('#player').setAttribute('position', position);
            }

        }
    }
    console.info('Walls added.');
});
